var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT a.*, c.company_name FROM address a ' +
        'LEFT JOIN company_address ca ON a.address_id = ca.address_id ' +
        'LEFT JOIN company c ON c.company_id = ca.company_id ' +
        'WHERE a.address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO address (street, zip_code) VALUES (?, ?)';

    var queryData = [params.street, params.zip_code];

    connection.query(query, queryData, function(err, result) {

            callback(err, result);
        });
};

exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ?, zip_code = ? WHERE address_id = ?';
    var queryData = [params.street, params.zip_code, params.address_id];

    connection.query(query, queryData, function(err, result) {
        addressCompanyDeleteAll(params.address_id, function(err, result){

            if(params.company_id != null) {
                addressCompanyInsert(params.address_id, params.company_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};


exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

var addressCompanyInsert = function(address_id, companyIdArray, callback){
    var query = 'INSERT INTO company_address (address_id, company_id) VALUES ?';

    var addressCompanyData = [];
    for(var i=0; i < companyIdArray.length; i++) {
        addressCompanyData.push([address_id, companyIdArray[i]]);
    }
    connection.query(query, [addressCompanyData], function(err, result){
        callback(err, result);
    });
};
module.exports.addressCompanyInsert = addressCompanyInsert;

var addressCompanyDeleteAll = function(address_id, callback){
    var query = 'DELETE FROM company_address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.addressCompanyDeleteAll = addressCompanyDeleteAll;
