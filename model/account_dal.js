/**
 * Created by johnm on 4/22/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'SELECT a.*, s.skill_name FROM account a ' +
        'LEFT JOIN account_skill ab ON a.account_id = ab.account_id ' +
        'LEFT JOIN skill s ON s.skill_id = ab.skill_id ' +
        'WHERE a.account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE ACCOUNT
    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?, ?, ?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {

        var account_id = result.insertId;
        var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
        var accountSkillData = [];
        for(var i=0; i < params.skill_id.length; i++) {
            accountSkillData.push([account_id, params.skill_id[i]]);
        }
        connection.query(query, [accountSkillData], function(err, result){
            callback(err, result);
        });
    });

};


exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE account SET email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        accountSkillDeleteAll(params.account_id, function(err, result){

            if(params.skill_id != null) {
                accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


var accountSkillInsert = function(account_id, skillIdArray, callback){

    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

    var accountSkillData = [];
    for(var i=0; i < skillIdArray.length; i++) {
        accountSkillData.push([account_id, skillIdArray[i]]);
    }
    connection.query(query, [accountSkillData], function(err, result){
        callback(err, result);
    });
};

module.exports.accountSkillInsert = accountSkillInsert;


var accountSkillDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

module.exports.accountSkillDeleteAll = accountSkillDeleteAll;
