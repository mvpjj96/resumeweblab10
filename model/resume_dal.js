/**
 * Created by johnm on 4/23/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT first_name, last_name, resume_name FROM account a ' +
        'RIGHT JOIN resume r ON r.account_id = a.account_id ORDER BY first_name; ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.accInfo = function(account_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?, ?)';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, queryData, function(err, result) {
            var resume_id = result.insertId;

            var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

            var resumeSchoolData = [];
            for(var i=0; i < params.school_id.length; i++) {
                resumeSchoolData.push([resume_id, params.school_id[i]]);
            }
            connection.query(query, [resumeSchoolData], function(err, result){

                var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

                var resumeCompanyData = [];
                for(var i=0; i < params.company_id.length; i++) {
                    resumeCompanyData.push([resume_id, params.company_id[i]]);
                }
                connection.query(query, [resumeCompanyData], function(err, result){

                    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

                    var resumeSkillData = [];
                    for(var i=0; i < params.skill_id.length; i++) {
                        resumeSkillData.push([resume_id, params.skill_id[i]]);
                    }
                    connection.query(query, [resumeSkillData], function(err, result){
                })
                callback(err, result);
                    });
                });
            });
};






