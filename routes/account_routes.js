/**
 * Created by johnm on 4/18/2017.
 */
var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var skill_dal = require('../model/skill_dal');

// View ALL accounts
router.get('/allAcc', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getById(req.query.account_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('account/accountViewById', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    skill_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('account/accountAdd', {'skill': result});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == '') {
        res.send('User email must be provided.');
    }
    else if(req.query.first_name == '') {
        res.send('First name must be provided. ');
    }
    else if(req.query.last_name == '') {
        res.send('Last name must be provided. ');
    }
    else if(req.query.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else {
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.redirect(302, '/account/allAcc');
            }
        });
    }
});


router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('A account id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result){
            res.render('account/accountUpdate', {account: result[0][0], skill: result[1]});
        });
    }

});

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
        res.redirect(302, '/account/allAcc');
    });
});

router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.redirect(302, '/account/allAcc');
            }
        });
    }
});

module.exports = router;
