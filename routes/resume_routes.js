/**
 * Created by johnm on 4/23/2017.
 */
var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

router.get('/add/selectuser', function(req, res){
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/chooseAcc', {'account': result});
        }
    });
});

router.get('/add', function(req, res){
    resume_dal.accInfo(req.query.account_id, function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {account_id: req.query.account_id, school: result[0], company: result[1], skill: result[2]});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.resume_name == '') {
        res.send('A resume name must be provided');
    }
    else if(req.query.school_id == null) {
        res.send('At least one school must be selected');
    }
    else if(req.query.company_id == null) {
        res.send('At least one company must be selected');
    }
    else if(req.query.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else {
        resume_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.send('success');
            }
        });
    }
});
module.exports = router;